package game;

import java.util.Random;

public class Mazo {
	
	private Carta[] cartas;
	private int repartidas;
	
	public Mazo() {
		cartas = new Carta[40];
		int k = 0;
		for (int p = 1; p <= 4; p++) {
			for (int n = 1; n <= 12; n++) {
				if (n != 8 && n != 9) {
					cartas[k++] = new Carta(p, n);
				}
			}
		}
		repartidas = 0;
	}
	
	// [0, 1, 2, 3, 4]
	// repartidas = 0
	//
	// [0, 1, 2, 3, 4]
	// repartidas = 1
	
	// [0, 1, 2, 3, 4]
	// repartidas = 4
	
	// [0, 1, 2, 3, 4]
	// repartidas = 5
	public void mostrar() {
		if (cartas.length == repartidas) {
			System.out.println("No hay más cartas, se repartieron todas.");
		}
		
		for (int i = repartidas; i < cartas.length; i++) {
			System.out.println(cartas[i]);
		}
	}
	
	// repartidas = 0 => cartas[0]
	// cartas[0] = null
	// repartidas = 1
	// devuelve c
	
	// repartidas = 1 => cartas[1]
	// cartas[1] = null
	// repartidas = 2
	// devueve c
	public Carta repartir() {
		Carta c = cartas[repartidas]; 
		cartas[repartidas++] = null;
		return c;
	}
	
	public void mezclar() {
		Random r = new Random();
		
		for (int k = 0; k < 5000; k++) {
			int i = repartidas + r.nextInt(cartas.length - repartidas);
			int j = repartidas + r.nextInt(cartas.length - repartidas);

			Carta aux = cartas[i];
			cartas[i] = cartas[j];
			cartas[j] = aux;
		}
	}
	
	// prueba
//	public static void main(String[] args) {
//		Mazo mazo = new Mazo();
//		
//		mazo.mezclar();
//
//		Carta cartaRepartida;
//		for (int i = 0; i < 10; i++) {
//			cartaRepartida = mazo.repartir();
//		}
//
//		mazo.mostrar();
//
//	}

}
