package game;

public class Carta {

	// Invariante de representación
	// ----------------------------
	//
	// 1 <= palo <= 4
	// 1 <= numero <= 12
	// numero != 8
	// numero != 9

	private int palo; // palo = 1, 2, 3, 4
	private int numero;

	public Carta(int palo, int numero) {
		if (palo < 1 || palo > 4 || numero < 1 || numero > 12 || numero == 8 || numero == 9) {
			throw new IllegalArgumentException("Valores inválidos: p = " + palo + " n = " + numero);
		}

		this.palo = palo;
		this.numero = numero;
	}

	public String toString() {
		String[] palos = { "espada", "basto", "oro", "copa" };
		return numero + " de " + palos[palo - 1];
	}

	// esto es de prueba
//	public static void main(String[] args) {
//		Carta c = new Carta(4, 5);
//		System.out.println(c);
//	}

}
